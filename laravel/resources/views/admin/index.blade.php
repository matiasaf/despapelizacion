@extends('layouts.app')

@section('estilos')

<link rel="stylesheet" href="{{url('/css/components/adminpage.css')}}">

@endsection

@section('content')

  @include('includes.navbar')

<div class="container" style="min-height:550px;">
  
  <br><br>

    <div class="row">
      
      <h2 class="mx-auto font-organismos-cargados">Organismos cargados</h2>
      
      <button @click="openModalAgregarOrganismo" class="btn pull-right">
              
          <i class="fa fa-plus" aria-hidden="true"></i>
          agregar organismo
                      
      </button>
      <button @click="openModalAgregarVotacion" class="btn pull-right">
              
          <i class="fa fa-plus" aria-hidden="true"></i>
          agregar votación
                      
      </button>
      
      <button @click="openModalHabilitarVotacion" class="btn pull-right">
              
          <i class="fa fa-plus" aria-hidden="true"></i>
          habilitar votación
                      
      </button>
    </div>

    <br>
    
    <div class="row">

      <div class="col-md-12">
      
        @include('admin.organismos-lista')
                  
      </div>
    
    </div>
    
    <div class="row">
        <h2 class="mx-auto font-organismos-cargados">Texto en la tarjeta verde</h2>
    </div>
    <br>
    <div class="row">

      <div class="col-md-12">
      
        @include('admin.texto-verde')
                  
      </div>
    
    </div>
   
    <br><br>
      

    @include('admin.modals.editar-organismo')
    @include('admin.modals.agregar-organismo')
    @include('admin.modals.agregar-votacion')
    @include('admin.modals.lista-votaciones')
    @include('admin.modals.editar-votacion')
  
 </div>



  {{-- @include('includes.boton-subir') --}}

  @include('includes.footer')

  {{-- modals --}}



@endsection

@section('scripts')

  @if(env('APP_ENV') === 'production')

    <script src="{{url('/js/adminpage-prod.js')}}"></script>

  @else

    <script src="{{url('/js/adminpage.js')}}"></script>

  @endif

@endsection
