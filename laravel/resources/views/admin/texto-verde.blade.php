<form>
    <div class="form-group row">
        <div class="col-sm-6 offset-sm-3">
            <textarea type="text" class="form-control" 
            id="texto_tarjeta_verde" 
            v-model="texto_tarjeta_verde"
            placeholder="Texto...">
            </textarea>
        </div>
    </div>
    <div class="form-group row">    
        <div class="col-sm-2 offset-sm-5">
            <button @click.prevent="editarTextTarjetaVerde" class="btn btn-secondary btn-block">Guardar</button>
        </div>
    </div>
</form>