<div class="card animated fadeIn">

    <div class="card-body" id="card-agregar-organismo">

        <div class="row">

            <div class="col-sm-12">

                <div class="form-group">
                    <label for="nombre" v-bind:class="{'texto-error': errores.nombre}">Ingresar nombre de organismo : </label>
                    <input type="text" class="form-control" name="nombre" v-model="nombre" v-bind:class="{ 'is-invalid': errores.nombre }">

                    <div class="invalid-feedback">
                        @{{errores.nombre}}
                    </div>

                </div>

            </div>

            <div class="col-sm-12">

                <div class="form-group">
                    <label for="objetivos_descripcion" v-bind:class="{'texto-error': errores.objetivos_descripcion}">Objetivos :</label>
                    <textarea class="form-control" v-bind:class="{ 'is-invalid': errores.objetivos_descripcion }" name="objetivos_descripcion"
                        v-model="objetivos_descripcion">
                    </textarea>
                    <div class="invalid-feedback">
                        @{{errores.objetivos_descripcion}}
                    </div>
                </div>

            </div>

            <div class="col-sm-12">

                <div class="form-group">
                    <label for="fecha_adhesion" v-bind:class="{'texto-error': errores.fecha_adhesion}">Fecha de adhesión : </label>
                    <input type="date" class="form-control" name="fecha_adhesion" v-model="fecha_adhesion" v-bind:class="{ 'is-invalid': errores.fecha_adhesion }">

                    <div class="invalid-feedback">
                        @{{errores.fecha_adhesion}}
                    </div>

                </div>

            </div>

            <div class="col-sm-12">

                <div class="form-group">
                    <label for="img_src" v-bind:class="{'texto-error': errores.img_src}">Link a la imagen :</label>
                    <input type="text" class="form-control" v-bind:class="{ 'is-invalid': errores.img_src }" name="img_src" v-model="img_src"
                    />
                    <div class="invalid-feedback">
                        @{{errores.img_src}}
                    </div>
                </div>

            </div>

        </div>

        <br>

        <div class="row">
            <button @click="agregarOrganismo" class="btn btn-outline-success mx-auto">Agregar</button>
        </div>

    </div>

</div>