<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="agregarVotacionModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Nueva votación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
        </div>
        <div class="modal-body">  
            <div class="row">

                    <div class="col-sm-12">
            
                        <div class="form-group">
                            <label for="votacion_nombre" v-bind:class="{'texto-error': errores.nombre}">Ingresar nombre de la votación : </label>
                            <input type="text" class="form-control" name="votacion_nombre" v-model="votacion_nombre" v-bind:class="{ 'is-invalid': errores.nombre }">
            
                            <div class="invalid-feedback">
                                @{{errores.nombre}}
                            </div>
            
                        </div>
            
                    </div>

                    <div class="col-sm-12">
            
                        <div class="form-group">
                        
                            <label for="votacion_fecha_inicio" v-bind:class="{'texto-error': errores.fecha_inicio}">Fecha de inicio : </label>
                            <input type="date" class="form-control" name="votacion_fecha_inicio" v-model="votacion_fecha_inicio" v-bind:class="{ 'is-invalid': errores.fecha_inicio }">
            
                            <div class="invalid-feedback">
                                @{{errores.fecha_inicio}}
                            </div>
                        
                        </div>
                    </div>
                    <div class="col-sm-12">
            
                        <div class="form-group">
                        
                            <label for="votacion_fecha_fin" v-bind:class="{'texto-error': errores.fecha_fin}">Fecha de fin : </label>
                            <input type="date" class="form-control" name="votacion_fecha_fin" v-model="votacion_fecha_fin" v-bind:class="{ 'is-invalid': errores.fecha_fin }">
            
                            <div class="invalid-feedback">
                                @{{errores.fecha_fin}}
                            </div>
                        
                        </div>
                    </div>
                

            
                </div>
            
                <br>
            
                <div class="row">
                    <button @click="agregarVotacion" class="btn btn-outline-success mx-auto">Agregar</button>
                </div>
            
            </div>
        </div>
      </div>
    </div>
