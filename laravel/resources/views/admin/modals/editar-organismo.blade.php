<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="editarOrganismoModal">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar organismo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre organismo: </label>
                            <input type="text" 
                            name="organismoEdit_nombre" 
                            class="form-control" 
                            placeholder="Ingrese nombre ..." 
                            v-model="organismoEdit_nombre">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="organismoEdit_institucion_apadrina">
                                Institución que apadrina : 
                            </label>
                            <input type="text" 
                            name="organismoEdit_institucion_apadrina" 
                            class="form-control" 
                            placeholder="Ingrese nombre ..." 
                            v-model="organismoEdit_institucion_apadrina">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Descripción de objetivos: </label>
                            <input type="text" 
                            name="organismoEdit_objetivos_descripcion" 
                            class="form-control" 
                            placeholder="Ingrese descripción ..." 
                            v-model="organismoEdit_objetivos_descripcion">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Fecha de adhesión: </label>
                            <input type="date" 
                            class="form-control" 
                            placeholder="Ingrese la fecha de adhesión ..." 
                            v-model="organismoEdit_fecha_adhesion">
                        </div>
                    </div>

                    <div class="col-md-12" v-if="organismoEdit_img_src">
                        <img :src="organismoEdit_img_src" class="img-fluid">
                    </div>

                    <div class="col-md-12" v-else>
                        <img :src="image" class="img-fluid">
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="imagen_institucion" v-if="!image">Cambiar imagen de institución :</label>
                            <input type="file" v-on:change="onFileChange" class="form-control" name="imagen_institucion">                            
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="pdf_compromisos">Ingresar pdf con los compromisos: </label>
                            <input type="file" class="form-control mx-auto" name="pdf_compromisos" id="pdf_compromisos" v-bind:class="{ 'is-invalid': errores.file_upload }" />
                            <div class="invalid-feedback">
                                @{{errores.file_upload}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button @click="editarOrganismo" class="btn btn-primary">Guardar cambios</button>
                <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>