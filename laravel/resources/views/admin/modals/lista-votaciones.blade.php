<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="habilitarListaVotacion">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Lista de votaciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">  
    
                 <div class="jumbotron">
                            <ul class="list-group" id="lista-organismos">
                                <li class="list-group-item" v-bind:class="{'activa' : votacion.activa == 1 , 'no-activa': votacion.activa == 0}" v-for="votacion in lista_votaciones">
                                    <div class="row">
                                        <div class="col-md-5">
                                           <b>
                                              <span style="font-size:1.3em;"> @{{votacion.nombre}} </span>
                                            </b> 
                                            <br>
                                            <b>Fecha inicio:</b> @{{ darFormato(votacion.fecha_inicio) }}
                                            <br>
                                            <b>Fecha fin:</b> @{{ darFormato(votacion.fecha_fin) }}
                                        </div>
                                        <div class="col-md-2">
                                            Estado : 
                                            <br>
                                            @{{votacion.activa == 1 ? 'activa': 'no activa'}}
                                        </div>
                                        <div class="col-md-5">
                                            <div class="btn-group" role="group">
                                                <button @click="eliminarVotacion(votacion)" class="btn btn-danger float-right">Eliminar</button>
                                                <button @click="openModalEditar(votacion)" class="btn btn-info float-right">Editar</button>
                                                <button @click="habilitarVotacion(votacion)" 
                                                class="btn btn-secondary float-right"
                                                v-if="votacion.activa == 0">
                                                Activar</button>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
            
            </div>
            </div>
          </div>
</div>
