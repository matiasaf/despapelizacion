<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="editarVotacionModal">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Editar votacion</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre votacion: </label>
                        <input type="text" 
                        name="organismoEdit_nombre" 
                        class="form-control" 
                        placeholder="Ingrese nombre ..." 
                        v-model="editar_votacion_nombre">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Fecha inicio: </label>
                        <input type="date" 
                        name="editar_votacion_fecha_inicio" 
                        class="form-control" 
                        v-model="editar_votacion_fecha_inicio">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Fecha fin: </label>
                        <input type="date" 
                        name="editar_votacion_fecha_fin" 
                        class="form-control" 
                        v-model="editar_votacion_fecha_fin">
                    </div>
                </div>

        </div>
        <div class="modal-footer">
            <button @click="editarVotacion" class="btn btn-primary">Guardar cambios</button>
            <button class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>
</div>