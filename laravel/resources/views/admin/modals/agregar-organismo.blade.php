

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="agregarOrganismoModal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo organismo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">  
                <div class="row">

                        <div class="col-sm-12">
                
                            <div class="form-group">
                                <label for="nombre" v-bind:class="{'texto-error': errores.nombre}">Ingresar nombre de organismo : </label>
                                <input type="text" class="form-control" name="nombre" v-model="nombre" v-bind:class="{ 'is-invalid': errores.nombre }">
                
                                <div class="invalid-feedback">
                                    @{{errores.nombre}}
                                </div>
                
                            </div>
                
                        </div>

                        <div class="col-sm-12">
                
                            <div class="form-group">
                                <label for="institucion_apadrina" v-bind:class="{'texto-error': errores.institucion_apadrina}">Ingresar institución que apadrina : </label>
                                <input type="text" class="form-control" name="institucion_apadrina" v-model="institucion_apadrina" v-bind:class="{ 'is-invalid': errores.institucion_apadrina }">
                
                                <div class="invalid-feedback">
                                    @{{errores.institucion_apadrina}}
                                </div>
                
                            </div>
                
                        </div>

                        <div class="col-md-12">
                            <img :src="image" class="img-fluid">
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group">

                                <label for="imagen_institucion" v-if="!image">Ingresar imagen de institución :</label>

                                <input type="file" v-on:change="onFileChange" class="form-control" name="imagen_institucion">
                                
                            </div>
                        </div>
                
                        <div class="col-sm-12">
                
                            <div class="form-group">
                                <label for="objetivos_descripcion" v-bind:class="{'texto-error': errores.objetivos_descripcion}">Objetivos :</label>
                                <textarea class="form-control" v-bind:class="{ 'is-invalid': errores.objetivos_descripcion }" name="objetivos_descripcion"
                                    v-model="objetivos_descripcion">
                                </textarea>
                                <div class="invalid-feedback">
                                    @{{errores.objetivos_descripcion}}
                                </div>
                            </div>
                
                        </div>
                
                        <div class="col-sm-12">
                
                            <div class="form-group">
                            
                                <label for="fecha_adhesion" v-bind:class="{'texto-error': errores.fecha_adhesion}">Fecha de adhesión : </label>
                                <input type="date" class="form-control" name="fecha_adhesion" v-model="fecha_adhesion" v-bind:class="{ 'is-invalid': errores.fecha_adhesion }">
                
                                <div class="invalid-feedback">
                                    @{{errores.fecha_adhesion}}
                                </div>
                            
                            </div>
                        </div>
                    

                        <div class="col-sm-12">

                            <div class="form-group">

                              <label for="pdf_compromisos">Ingresar PDF con los compromisos :</label>
                        
                              <input type="file" class="form-control mx-auto" name="pdf_compromisos" id="pdf_compromisos" v-bind:class="{ 'is-invalid': errores.file_upload }" />
                        
                              <div class="invalid-feedback">
                                @{{errores.file_upload}}
                              </div>
                        
                            </div>
                        
                        </div>
                
                    </div>
                
                    <br>
                
                    <div class="row">
                        <button @click="agregarOrganismo" class="btn btn-outline-success mx-auto">Agregar</button>
                    </div>
                
                </div>
            </div>
          </div>
        </div>
      </div>
      

