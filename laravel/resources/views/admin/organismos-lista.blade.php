

<div class="jumbotron">
    <ul class="list-group" id="lista-organismos">
        <li class="list-group-item" v-for="organismo in organismos">
            <div class="row">
                <div class="col-md-9">
                    @{{organismo.nombre}}
                </div>
                <div class="col-md-3">
                    <div class="btn-group" role="group">
                        <button @click="openModalEditarOrganismo(organismo)" class="btn btn-secondary float-right">Editar</button>
                        <button @click="eliminarOrganismo(organismo)" class="btn btn-danger float-right">Eliminar</button>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>