<div @click="traerObjetivos" class="card card-organismo-ganador animated fadeIn" style="width: 25rem;">
        
    <img class="card-img-top" :src="organismo_ganador.img_src" alt="Card image cap">
        
    <div class="card-body">

        <h5 class="card-title-ganador"> @{{organismo_ganador.institucion_apadrina}}</h5>

    </div>
</div>