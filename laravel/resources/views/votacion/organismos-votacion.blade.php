<div v-if="habilitoGrafico">
    
    <div class="row">

        <h3 style="text-align:center" class="mx-auto">¡Gracias por dejar su voto!</h3>
    </div>

    <br><br>
    
    <div class="row">

            <div class="grafico">
                <canvas class="animated fadeIn" id="myChart"></canvas>
            </div>
    </div>

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <button 
            @click="volverAlSitio"
            type="button" 
            class="btn btn-lg btn-block boton-seguir">
            Seguir en el sitio
            </button>
        </div>
    </div>
    
</div>

<div v-if="habilitoMensajeAntesDeVotacion">

        <div class="row">

            <div class="col-md-6 offset-md-3">

                <h4 style="margin-top:40%; margin-bottom:20%; padding:10%;">La votación comienza el día lunes 3 de diciembre.</h4>

            </div>
        
        </div>


        <div class="row">
            <div class="col-md-6 offset-md-3">
                <button 
                    @click="volverAlSitio"
                    type="button" 
                    class="btn btn-lg btn-block boton-seguir">
                
                    Seguir en el sitio
                
                </button>
            </div>
        </div>

</div>

<div v-if="habilitoMensajePosVotacion">

        <div class="row">
            <span class="titulo-organismo-ganador mx-auto" style="color: #03a7e0">
                ¡Votación finalizada!     
            </span>
        </div>
        <br><br>
        <div class="row">
              <span class="titulo-organismo-ganador mx-auto">
                 La institución ganadora con el <span style="color:red;"> @{{ Math.floor( ( parseInt(organismo_ganador.cantidad_votos) )  ) }} %  </span> de los votos  es :     
            </span>


        </div>

        <br><br>

        <div class="row">
         
            <div class="mx-auto">
         
                @include('votacion.organismo-card-ganador')
         
            </div>
       
        </div>

        <br><br>
        
        <div class="row animated fadeInLeft" v-if="habilitoObjetivos">
            <div class="col-md-4 offset-md-4 font-descripcion">
               <b> Objetivos propuestos :</b>
                <br>
                <br>
                <p>
                    
                    @{{organismo_ganador.objetivos_descripcion}}
                </p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-6 offset-md-3">
            
                <button 
                    @click="volverAlSitio"
                    type="button" 
                    class="btn btn-lg btn-block boton-seguir">
                
                    Seguir en el sitio
                
                </button>
            </div>
        </div>

</div>

<div class="organismos_section" v-if="mostrarInstitucionesAVotar">
    
    <br>

    <div class="row">
        
        <h2 class="font-title-organismos mx-auto">Elegir institución</h2>
        
    </div>

    <br><br>
        
    <div class="row">
        
        <div class="col-md-3" v-for="organismo in organismos" style="margin-top:2%;">
            
            @include('votacion.organismo-card')
            
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <button 
            @click="guardarVotacion"
            type="button" 
            class="btn btn-lg btn-block boton-seguir">
            Votar
            </button>
        </div>

    </div>


</div>

 