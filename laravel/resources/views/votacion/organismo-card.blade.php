
  <div class="flip">
    <div @click="votar(organismo)" class="card" v-bind:class="{ 'flipped' : organismo.voto}" style="width: 100%;"> 
      <div class="face front"> 
        <div class="inner" v-bind:style="{ backgroundImage: 'linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), url(' + organismo.img_src + ')' }">     
           <span class="font-intitucion">
              @{{organismo.institucion_apadrina}}
          </span> 
        </div>
      </div> 
      <div class="face back"> 
        <div class="inner-tick"> 
            <i class="fa fa-check-circle-o fa-3x" aria-hidden="true"></i>
        </div>
      </div>
    </div>	 
  </div>