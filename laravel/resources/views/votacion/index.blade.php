@extends('layouts.app')

@section('title', 'Votación de institución')

@section('estilos')

  <link rel="stylesheet" href="{{url('/css/components/votacion.css')}}">
 
  {{-- css imports para el formato de pagina de la oficina --}}
  <link rel="stylesheet" href="{{url('/css/components/menu_based.css')}}">
  <link rel="stylesheet" href="{{url('/css/components/based.css')}}">

@endsection

@section('content')

  {{-- @include('components.navbar.navbar') --}}
  
  {{-- @include('includes.header-despapelizacion') --}}

  <div class="container-votacion" >
    <div class="container" v-cloak>
      
      <br><br>
      
      @include('votacion.organismos-votacion')
      
      @include('components.modals.compromisos-mostrar')
      
      
      <form style="display:none" action="{{route('compromisos.descargar')}}" method="post" id="form_descargar_compromisos">
          
          <input type="hidden" name="_token" value="{{csrf_token()}}">
          
          <input type="text" name="id" v-model="organismoSeleccionado.id">
          
        </form>
        
      </div>
    </div>
      
      {{-- @include('includes.footerwrap') --}}
      
      {{-- @include('includes.pie') --}}
      
      {{-- @include('includes.boton-subir') --}}
      
      
      @endsection
      
      @section('scripts')
      
      
      @if( env('APP_ENV') === 'production' )
      
      <script src="{{url('/js/votacion-prod.js')}}"></script>
      
      @else
      
      <script src="{{url('/js/votacion.js')}}"></script>
      
      @endif
      
      <script src="{{url('/js/menu.js')}}"></script>
      
      @endsection
      