<div id="footerwrap">
	<div class="container">
	  <div class="row">
		   <div class="col-lg-7 col-md-7 col-sm-10 col-xs-10">
				 <h8 class="font-nexa-bold" style="color: #799f4f;">CONTACTO</h8>
				   <div class="titulos_pie"><span style="width: 15px; font-size: 1rem;" class="glyphicon glyphicon-map-marker hidden-sm hidden-xs"></span> San Martín 655</div>
				   <div class="titulos_pie"><span style="width: 15px; font-size: 1rem;" class="glyphicon glyphicon-phone-alt hidden-sm hidden-xs"></span> 0343-4072732</div>
				   <div class="titulos_pie"><span style="width: 15px; font-size: 1rem;" class="fa fa-twitter"></span> @ModernizacionER</div>
				   <div class="titulos_pie"><span style="width: 15px; font-size: 1rem;" class="fa fa-facebook"></span> ModernizacionER</div>
				   <div class="titulos_pie"><span style="width: 15px; font-size: 1rem;" class="glyphicon glyphicon-envelope hidden-sm hidden-xs"></span><a href="mailto:secmodernizacioncyt@entrerios.gov.ar"> secmodernizacioncyt@entrerios.gov.ar</a></div>
		   </div>


		   <div class="col-lg-4 col-md-4 hidden-sm hidden-xs" style="padding: 25px;">
				 <div align="right"><img src="{{url('img/pie_sitio_despapelizacion.png')}}" class="img-responsive" alt="Logo"></div>
		   </div>
	 </div>
 </div>
</div>