@extends('layouts.app')

@section('estilos')
<style media="screen">
.boton {

  width: 150px;
  background-color: #006986;
  color:white;

}

</style>
@endsection

@section('content')

<div class="container" style="height:450px;margin-top:100px;">

  <form action="{{route('auth.login')}}" method="post">

    <br>

    @if(session('message'))
      <div class="alert alert-danger" role="alert">
        {{ session('message') }}
      </div>
    @endif

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="row justify-content-md-center">

      <div class="col-sm-4">

        <div class="form-group fontNexa">
          <label for="username">Nombre de usuario:</label>
          <input type="text" class="form-control" name="username" id="username">
        </div>

      </div>

    </div>

    <div class="row justify-content-md-center">

      <div class="col-sm-4">

        <div class="form-group fontNexa">
          <label for="password">Password:</label>
          <input type="password" class="form-control" name="password" id="password">
        </div>

      </div>

    </div>

    <br>

    <div class="row">
        <button type="submit" class="btn boton mx-auto fontNexa">Acceder</button>
    </div>

  </form>

</div>

@include('includes.footer')

@endsection
