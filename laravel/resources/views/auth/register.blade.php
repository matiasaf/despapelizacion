@extends('layouts.app')

@section('content')

<div class="container">

<form action="{{route('auth.register')}}" method="post">

  <input type="hidden" name="_token" value="{{ csrf_token() }}">

  <br>

  <div class="form-group">
    <label for="exampleInputEmail1">E-mail</label>
    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">No vamos a compartir el mail con nadie más.</small>
  </div>
  <div class="form-group">
    <label for="username">Nombre de usuario:</label>
    <input type="text" name="username" class="form-control" id="username">
  </div>

  <div class="form-group">
    <label for="nombre">Nombre:</label>
    <input type="text" name="nombre" id="nombre" class="form-control">
  </div>

  <div class="form-group">
    <label for="apellido">Apellido:</label>
    <input type="text" name="apellido" id="apellido" class="form-control">
  </div>

  <div class="form-group">
    <label for="password">Password:</label>
    <input type="password" class="form-control" name="password" id="password" >
  </div>

  <div class="form-group">
    <label for="password_confirm">Confirmar password:</label>
    <input type="password" class="form-control" name="password_confirm" id="password_confirm" >
  </div>

  <button type="submit" class="btn btn-primary">Registrar</button>

</form>

</div>


@endsection
