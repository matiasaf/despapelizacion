@extends('layouts.app')

@section('title', 'Despapelizacion')

@section('estilos')

  <link rel="stylesheet" href="{{url('/css/components/despapelizacion.css')}}">
 
  {{-- css imports para el formato de pagina de la oficina --}}
  <link rel="stylesheet" href="{{url('/css/components/menu_based.css')}}">
  <link rel="stylesheet" href="{{url('/css/components/based.css')}}">

@endsection

@section('content')

  
  @include('components.navbar.navbar')
  
  @include('includes.header-despapelizacion')

  <div class="container">

    {{-- @include('despapelizacion.encuesta') --}}

    <br>

    <template v-if="votacion_activa">

        <div class="col-md-12">
            <a href={{url('/votacion')}}>
                <button type="button" style="margin-top:10px" class="btn btn-lg btn-block boton-otros-votacion">
                    <div class="font-boton-otros">
                        Votación en curso - ¡Votá !
                    </div>
                </button>
            </a>
        </div>

    </template>

    <br>

    @include('components.main-card')

    
    @include('components.buenas-practicas')

    <br><br><br><br>

    @include('components.acordion')

    <br><br>

    <div class="sabias_que_section">
          
        @include('components.card-verde')
        
    </div>

    <br><br>

    @include('components.sumate-card')

    @include('components.organismos-section')

    <br><br>
    
    @include('components.boton-sumate')
    
    <br><br>

    @include('components.boton-sumate-flotante')

    @include('components.modals.describir-cards')
    
    @include('components.modals.adhesion-form')

    @include('components.modals.compromisos-mostrar')

    
    <form style="display:none" action="{{route('compromisos.descargar')}}" method="post" id="form_descargar_compromisos">

        <input type="hidden" name="_token" value="{{csrf_token()}}">
    
        <input type="text" name="id" v-model="organismoSeleccionado.id">
    
    </form>
              
  </div>

  @include('includes.footerwrap')

  @include('includes.pie')

  @include('includes.boton-subir')


@endsection

@section('scripts')


  @if( env('APP_ENV') === 'production' )

    <script src="{{url('/js/despapelizacion-prod.js')}}"></script>

  @else

    <script src="{{url('/js/despapelizacion.js')}}"></script>

  @endif

  <script src="{{url('/js/menu.js')}}"></script>

@endsection
