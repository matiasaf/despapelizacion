<!doctype html>
<html lang="en">

<head>

  {{-- <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" /> --}}

  <!-- Required meta tags -->
  <meta charset="utf-8" name="charset">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

  {{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> --}}
  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

  {{-- font awesome v4 --}}
  {{-- <link rel="stylesheet" href="{{url('css/font-awesome-4.min.css')}}"> --}}

  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

  <link rel="stylesheet" href="{{ url('./css/animate.css') }}">
  <link rel="stylesheet" href="{{ url('./css/main.css') }}">

  @yield('estilos')

  <title>@yield('title')</title>

</head>

<body>

  <div id="manage-vue">

    @yield('content')

  </div>

  <script src="{{url('assets/js/axios.min.js')}}"></script>
  <script src="{{url('assets/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{url('assets/js/underscore-min.js')}}"></script>

  <script src="{{url('assets/js/popper.min.js')}}"></script>
  <script src="{{url('assets/js/bootstrap.min.js')}}"></script>

  
  {{--
    <script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> --}}
    
    {{-- <script src="https://ssense.github.io/vue-carousel/js/vue-carousel.min.js"></script> --}}
    

  @if ( env('APP_ENV') == 'production' )

    <script src="{{url('assets/js/vue-prod.min.js')}}"></script>

  @else

    <script src="{{url('assets/js/vue-dev.js')}}"></script>

  @endif

  <script src="{{url('assets/js/toastr.min.js')}}"></script>

  <script src="{{url('assets/js/moment.min.js')}}"></script>
  
  <script src="{{url('assets/js/chart.bundle.min.js')}}"></script>

  <script src="{{url('js/libs/functions.js')}}"></script>

  <script src="{{url('js/boton-subir.js')}}"></script>

  <script src="{{url('assets/js/validator.min.js')}}"></script>




  <script type="text/javascript">
    axios.defaults.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
  </script>

  @yield('scripts')

</body>

</html>
