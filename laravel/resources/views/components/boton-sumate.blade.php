<div class="row">
    <div class="col-md-12">
        <button @click="habilitarModalInscripcion" type="button" style="margin-top:10px" class="btn btn-lg btn-block boton-sumate">
            <div class="font-boton-sumate">
                Sumate
            </div>
        </button>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <a href="{{url('img/manual_buenas_practicas.pdf')}}">
                <button type="button" style="margin-top:10px" class="btn btn-lg btn-block boton-otros-manual">
                    <div class="font-boton-otros">
                        Manual de buenas prácticas
                    </div>
                </button>
            </a>
    </div>
    <div class="col-md-6">
        <a href={{url('/votacion')}}>
            <button type="button" style="margin-top:10px" class="btn btn-lg btn-block boton-otros-votacion">
                <div class="font-boton-otros">
                    Votación
                </div>
            </button>
        </a>
    </div>
</div>