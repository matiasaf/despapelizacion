<!DOCTYPE html>
<html>

<head>
	<title></title>
</head>

<body>

	<div style="width: 100%; position: relative; min-height: 1px; display: block; box-sizing: border-box; float: left;background-color:#f0f0f0">
		<div class="row">

			<img src="https://image.ibb.co/f9YbDL/enca-despapelizacion.png" style="width:100%; position: relative;">

			<div style="width: 100%; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; display: block; box-sizing: border-box; float: left;">

						<div style="padding-top: 5px; padding-bottom: 10px; padding: 15px; box-sizing: border-box; display: block; -webkit-tap-highlight-color: rgba(0,0,0,0); ">

						<div style="margin-right: -15px; margin-left: -15px; box-sizing: border-box; display: block;">

                        <p style="font-family: 'Century Gothic'; text-align: center; font-size: 28px; color:black;">
                            El organismo {{$data['nombre_organismo_formulario']}} esta intentando registrarse.
                        </p>
                        <br><br>

                        <p style="font-family: 'Century Gothic'; text-align: center; font-size: 20px; color:black;">
                               Datos :
                               <br>
                               <b>Nombre : </b> {{$data['nombre_organismo_formulario']}}
                               <br>
                               <b>Autoridad del organismo : </b> {{$data['autoridad_organismo_formulario']}}
                               <br>
                               <b>Persona a contactar : </b> {{$data['persona_contacto_formulario']}}
                               <br>
                               <b>Correo electrónico : </b> {{$data['correo_electronico_formulario']}}
                               <br>
                               <b>Teléfono de contacto : </b> {{$data['telefono_contacto_formulario']}}
                               <br> 
                               <b>Título del compromiso : </b> {{$data['titulo_compromiso_formulario']}}
                               <br> 
                               <b>Detalle breve del compromiso : </b> {{$data['detalle_compromiso_formulario']}}
                               <br> 
                        </p>
				
						</div>

					</div>

		</div>

	</div>

</body>

</html>