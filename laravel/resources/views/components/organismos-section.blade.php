<div class="organismos_section">
    
    <br><br><br>

    <div class="row">
        
        <h2 class="font-title-organismos mx-auto">Organismos adheridos</h2>
        
    </div>
        
    {{-- <div class="row">
        
        <div class="col-md-4" v-for="organismo in organismos" style="margin-top:2%;">
            
            @include('components.organismo-card')
            
        </div>
        
    </div> --}}

    <br>

    <div class="row" v-if="mostrar_organismos">
        
         {{-- FLECHA IZQUIERDA DEL CARROUSEL CASERO --}}
      
           <div class="col-md-1 d-flex align-items-center">
             <i @click="retrazoCarrousel" class="fa fa-2x fa-arrow-left mx-auto" aria-hidden="true" style="color: #78ab4d;"></i>
           </div>
           
           <div class="col-md-10">
               
               <div class="row" style="margin-left:2%">      
                
                  {{-- tarjetas con las buenas practicas --}}
                  
                   <div  style="margin-top:5%" class="col-md-4 d-flex align-items-center" v-for="organismo in organismos[pagina]">
                       
                     @include('components.organismo-card')
        
                   </div>
           
                </div>
        
               <br><br>
        
                <div class="row">
        
                    <div class="botones-slider mx-auto">
                        
                    <i v-bind:class="{ 'fa fa-circle': _pagina == pagina + 1 , 'fa fa-circle-o': _pagina != pagina + 1 }" v-for="_pagina in total_paginas" aria-hidden="true"></i>               
                              
                    </div>
        
                </div>
            
               <br>
                
            </div>
                       
            {{-- FLECHITA DERECHA DEL CARROUSEL CASERO --}}
        
            <div class="col-md-1 d-flex align-items-center">
                <i @click="avanzoCarrousel" class="fa fa-2x fa-arrow-right mx-auto" aria-hidden="true" style="color: #78ab4d; margin-left:20px;"></i>
            </div>
           
    </div>
</div>