<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"  aria-hidden="true" id="compromisosModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title">
               <div class="font-modal-title">
                 @{{organismoModal.nombre}}
                </div> 
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
           
          <p class="font-modal">
              
              @{{organismoModal.objetivos_descripcion}}
        
          </p>
          
        </div>
  
      </div>
    </div>
  </div>