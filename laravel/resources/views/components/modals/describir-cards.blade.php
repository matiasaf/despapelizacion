<!-- Large modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="descripcionCardsModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">
             <div class="font-modal-title">
               @{{modalCard.titulo}}
              </div> 
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
         
        <p class="font-modal" v-html="modalCard.descripcion">
            
            @{{modalCard.descripcion}}
      
        </p>
        
      </div>

    </div>
  </div>
</div>