<!-- Large modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="adhesionFormModal">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">
                    <div class="font-modal-title">
                     Formulario de adhesión
                    </div> 
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>
            
              <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="nombre-organismo" class="col-form-label font-modal-adhesion">Nombre del organismo:</label>
                                    <input type="text" class="form-control" id="nombre-organismo" v-model="nombre_organismo_formulario">
                                </div>
                                <div class="form-group">
                                    <label for="autoridad-organismo" class="col-form-label font-modal-adhesion">Autoridad del organismo:</label>
                                    <input type="text" class="form-control" id="autoridad-organismo" v-model="autoridad_organismo_formulario">
                                </div>
                                <div class="form-group">
                                    <label for="persona-contacto" class="col-form-label font-modal-adhesion">Persona a contactar:</label>
                                    <input type="text" class="form-control" id="persona-contacto" v-model="persona_contacto_formulario">
                                </div>
                                <div class="form-group">
                                    <label for="correo-electronico" class="col-form-label font-modal-adhesion">Correo electrónico:</label>
                                    <input type="text" class="form-control" id="correo-electronico" v-model="correo_electronico_formulario">
                                </div>
                                <div class="form-group">
                                    <label for="telefono-contacto" class="col-form-label font-modal-adhesion">Teléfono de contacto:</label>
                                    <input type="text" class="form-control" id="telefono-contacto" v-model="telefono_contacto_formulario">
                                </div>
                            
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="titulo-compromiso" class="col-form-label font-modal-adhesion">Título del compromiso:</label>
                                    <input type="text" class="form-control" id="titulo-compromiso" v-model="titulo_compromiso_formulario">
                                </div>
                                <div class="form-group">
                                    <label for="detalle-compromiso" class="col-form-label font-modal-adhesion">Detalle breve del compromiso:</label>
                                    <textarea  style="height:200px;"class="form-control" id="detalle-compromiso" v-model="detalle_compromiso_formulario"></textarea>
                                </div>
                            </div>
                                
                            </div>
                            
                        </form>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary font-modal-adhesion boton-cancelar" data-dismiss="modal">Cancelar</button>
                        <button @click="enviarFormulario" type="button" class="btn boton-adherirse font-modal-adhesion">Adherirse</button>
                    </div>
                </div>
            
            </div>
      
        </div>
      </div>