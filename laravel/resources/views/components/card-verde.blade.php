<div class="row">

    <div class="col-md-12">

        <div class="card" id="tarjeta-verde">

            <div class="card-body">

                <div class="row">

                    <div class="col-md-9">

                        <div class="font-title-tarjeta-verde">
                            ¿Sabias qué?
                        </div>

                        <br>

                        <ul>

                            <li class="font-list-tarjeta-verde">
                                Para fabricar 1 tonelada de papel se necesitan :
                                <br> - 7.000 kilovatios por hora.
                                <br> - Talar 15 árboles.
                                <br> - Entre 150.000 a 200.000 litros de agua.
                            </li>
                            <br>
                            <li class="font-list-tarjeta-verde">
                                La industria del papel es la quinta industria mundial de consumo de energía.
                            </li>
                            <br>
                            <li class="font-list-tarjeta-verde">
                                Con un árbol se hacen 22 resmas de papel.
                            </li>

                        </ul>

                    </div>

                    <div class="col-md-3 d-flex align-items-center">
                    
                        {{-- <img class="img-fluid img-despa" src="{{url('img/img_despa.png')}}" alt=""> --}}
                        
                        <p class="gree-card-texto" v-html="green_card_texto">
                            @{{green_card_texto}}
                        </p>

                    </div>

                </div>
            </div>

        </div>
    </div>

</div>