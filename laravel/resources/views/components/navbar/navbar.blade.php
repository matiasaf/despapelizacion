<nav class="barra-nav navbar navbar-expand-lg fixed-top">
        
  <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item font-navbar">
              <a class="nav-link" @click="scrollToBuenasPracticas" href="#">
                  <span v-bind:class="{ 'font-selected': buenas_practicas_active }">Buenas prácticas</span> 
                  <span class="sr-only"></span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link font-navbar"  href="#">
                <span style="color:white;">-</span>
                <span class="sr-only"></span>
              </a>
            </li>
            <li class="nav-item font-navbar">
                <a class="nav-link font-navbar" @click="scrollToSabiasQue" href="#">
                    <span v-bind:class="{ 'font-selected': sabias_que_active }">¿Sabias qué?</span>
                    <span class="sr-only"></span>
                  </a>
            </li>
            <li class="nav-item">
                <a class="nav-link font-navbar"  href="#">
                  <span style="color:white;">-</span>
                  <span class="sr-only"></span>
                </a>
            </li>
            <li class="nav-item font-navbar">
                <a class="nav-link" @click="scrollToOrganismos" href="#">
                    <span v-bind:class="{ 'font-selected': organismos_active }" >Organismos adheridos</span>
                    <span class="sr-only"></span>
                  </a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link font-navbar"  href="#">
                <span style="color:white;">-</span>
                <span class="sr-only"></span>
              </a>
            </li>

            <li class="nav-item font-navbar">
                <a class="nav-link" href="{{url('votacion')}}">
                    <span v-bind:class="{ 'font-selected': votacion_active }" >Votá</span>
                    <span class="sr-only"></span>
                  </a>
            </li>

          </ul>

        </div>
        
</nav>