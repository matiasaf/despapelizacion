
<div class="accordion">
        <div class="card">

          <div class="card-header" id="headingOne">

            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Programa
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">

                  El programa es coordinado por la Secretaría de Modernización del Estado, Ciencia y Tecnología,
                  dependiente de la Secretaría General de la Gobernación y cuenta con la colaboración de la Dirección
                  Provincial de Vialidad, encargada del traslado del papel de descarte recolectado a la Papelera Entre
                  Ríos (que realiza el pesaje de lo recibido otorgando el correspondiente certificado de destrucción del
                  papel) y de la fundación “Quanta El Bien Hacer”, que pone a disposición el mobiliario para donar a las
                  distintas instituciones públicas y/o sociales apadrinadas por quienes adhieren al programa, así como
                  de la Dirección General de Mantenimiento de la Casa de Gobierno de Entre Ríos.
                  Al momento del lanzamiento el Programa contó con la colaboración de la Secretaría de Juventud y
                  de la Secretaría de Ambiente.

              </div>
            </div>
          </div>


        <div class="card">
          <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Organismos y/o actores participantes
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">


                Secretaría de Modernización, Dirección Provincial de Vialidad, Dirección General de Mantenimiento de Casa de Gobierno y Quanta Reciclaje.

            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="headingThree">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Objetivos del programa
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>
          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">


                <b>1 -</b> Propiciar la despapelización dentro de la administración pública provincial para generar un
                entorno de trabajo saludable.
                <br>
                <b>2 -</b> Disminuir la utilización del papel mediante la adopción de buenas prácticas.
                <br>
                <b>3 -</b> Favorecer a diversas instituciones públicas y/o sociales con mobiliario financiado a través del
                papel de descarte.
                <br>
                <b>4 -</b> Contribuir con el cuidado del ambiente



            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-header" id="heading4">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Destinatarios
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">

              <i class="fa fa-check" aria-hidden="true"></i> Organismos u oficinas de la administración pública provincial.

                <br><br>

                La primera etapa de implementación se llevará a cabo en las oficinas ubicadas en la Casa de
                Gobierno y en la Dirección Provincial de Vialidad por razones de logística y debido a que estos
                ámbitos son productores de la mayor cantidad de papel y responsables de su concentración.
                Esta primera fase se entenderá como una “prueba piloto” a ser implementada de manera
                progresiva en todas las oficinas públicas provinciales.
            </div>
          </div>

        </div>

        <div class="card">
          <div class="card-header" id="heading5">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Estudio de diagnóstico
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">

                Durante el 2016 la actual Secretaría de Modernización del Estado, Ciencia y Tecnología, realizó un
                estudio en más de 150 oficinas de la administración pública provincial con el objetivo de identificar
                las oportunidades de mejora que permitan garantizar el derecho ciudadano a una gestión pública
                de calidad.
                <br>
                Una de las primeras conclusiones a las cuales se arribó es que el Estado entrerriano registra un
                enorme volumen de actos administrativos en formato papel, implicando que más del 60% de las
                oficinas visitadas evidencie problemas vinculados al uso del espacio físico ocupado con
                almacenamiento de expedientes vivos y archivos históricos. Algunos de los números que arrojó el
                estudio son:
                <br>
                La Dirección General de Recursos Humanos registra 250 mil legajos históricos archivados, lo que
                representa un número aproximado de cinco millones de fojas.
                Sólo en el Sistema de Gestión de Trámites de la administración pública se generan alrededor de
                siete mil expedientes mensuales, con un promedio de 25 fojas cada uno, lo que resulta en 175.000
                hojas por mes. Esto es equivalente a 350 resmas de hojas, 350 resmas de hojas de una densidad
                de 75-80g/m2 suman casi 1 tonelada de peso. Sin embargo, este resultado no implica el
                movimiento diario de otros tipos de actos administrativos tales como expedientes internos, notas
                simples, de compras, pedidos de licencia, informes, resoluciones, comunicaciones, fotocopias u
                otros.
                <br>
                La administración pública provincial posee a la fecha 1.970.000 expedientes, lo que equivale a un
                total de casi cincuenta millones de hojas, obtenidos del promedio de 25 fojas cada uno. Por
                ejemplo, el Ministerio de Gobierno y Justicia utiliza un promedio de 350 resmas mensuales, es
                decir, una tonelada de papel por mes.
            </div>
          </div>

        </div>

        <div class="card">
          <div class="card-header" id="heading6">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Abordaje
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">
               <b> El programa comprende dos abordajes:</b>
               <br><br>
                <b>1-</b> El proceso de clasificación y separación del papel de descarte.
                <br>
                <b>2-</b>  Elaboración de la Adhesión Institucional para la adopción de buenas prácticas en la
                reducción de la utilización del papel.
            </div>
          </div>

        </div>

        <div class="card">
          <div class="card-header" id="heading7">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Proceso de clasificación
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">

                Para comenzar los organismos de la administración pública provincial son los encargados de adecuar las cajas para el acopio del papel de descarte.

                  Simultáneamente se realizará una campaña de
                comunicación y concientización para lograr la sensibilización de los agentes involucrados acerca de
                la importancia del cuidado del ambiente y de la manera en que podemos accionar al respecto
                desde el Estado.

                <br>
                <br>

                <div class="acordion-aclaracion">

                  <b>ACLARACIÓN:</b> El papel de descarte incluye papel blanco, de color, cartón, diarios, folletos y
                  revistas, entre otros. Es importante que éste no contenga yerba, alimentos, bebidas u otros
                  elementos que pudieran humedecerlo.

                </div>

                <br>
                Con una frecuencia mensual (a convenir con la Dirección Provincial de Vialidad) se retirará el papel
                que se haya recolectado en las cajas ubicadas en los lugares compartidos y se lo trasladará a la
                Papelera Entre Ríos, que procederá a realizar su pesaje y a otorgar el certificado de destrucción
                correspondiente.
                <br>
                Una vez determinada la cantidad de dinero disponible a través de la entrega del papel en la
                Papelera de Entre Ríos, se trabajará en conjunto con la fundación “Quanta El Bien Hacer”, quien
                pondrá a disposición mobiliario elaborado con plástico reciclado para ser donado cada tres meses
                a entidades sociales o instituciones públicas seleccionados mediante un sistema de votación
                disponible en esta misma plataforma <a href="https://www.entrerios.gov.ar/modernizacion/despapelizacion/votacion"> (https://www.entrerios.gov.ar/modernizacion/despapelizacion/votacion )</a>.
                Cada organismo que participe del programa podrá elegir una institución a la cual apadrinar para
                que sea una de las destinatarias del mobiliario.
              </div>
          </div>

        </div>

        <div class="card">
          <div class="card-header" id="heading8">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Adhesión Institucional de Despapelización
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">

                Los organismos que se sumen a la iniciativa serán visitados por la Secretaría de Modernización del
                Estado, Ciencia y Tecnología con el objetivo de redactar en conjunto la Adhesión
                Institucional de Despapelización. Su elaboración debe dejar asentadas las acciones diarias que
                implican un uso indiscriminado de papel en sus oficinas según los agentes de esa institución. A
                partir de esa observación se redactará una carta con compromisos para implementar buenas
                prácticas vinculadas a su utilización.
                <br>

                La Adhesión de Despapelización será un documento que contribuirá a ordenar y
                optimizar los procesos del organismo en materia de despapelización, en el que se establecerán
                objetivos generales y específicos con plazos determinados. Esta carta deberá comenzar con un
                diagnóstico del papel utilizado en los últimos años para poder definir compromisos reales.

                <br>
                La Secretaría acompañará a las instituciones en el proceso de despapelización y de acuerdo al
                nivel de avance otorgará el sello de Calidad de Despapelización en formato digital y un cartel que
                podrá ser exhibido en los ingresos de las oficinas. Tanto la carta como el sello son diseñados con el
                objetivo de lograr el compromiso de los actores y a su vez para que los organismos públicos que
                participen puedan comunicarle a la ciudadanía (a través de sus páginas oficiales) que son parte del
                programa.
            </div>
          </div>

        </div>

        <div class="card">
          <div class="card-header" id="heading9">
            <h5 class="mb-0">
              <a class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
                <div class="row" id="acordion-despapelizacion">
                  <div class="col-sm-11">
                    <div class="font-acordion">
                      Colaboran con el descarte de papel
                    </div>
                  </div>
                  <div class="col-sm-1">
                    <i class="fa fa-chevron-up"></i>
                  </div>
                </div>
              </a>
            </h5>
          </div>

          <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordionExample">
            <div class="card-body font-acordion-body">

              <li>Tesorería de la Provincia</li>
              <li>Secretaría Privada de la Gobernación</li>
              <li>Dirección General de Despacho de la Secretaría de la Producción</li>
              <li>Dirección General de Informática</li>
              <li>Dirección de Análisis y Programación Económica MEHyF</li>

            </div>
          </div>

        </div>

      </div>
