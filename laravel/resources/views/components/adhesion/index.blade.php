@extends('layouts.app')

@section('title', 'Despapelizacion')

@section('estilos')

  <link rel="stylesheet" href="{{url('/css/components/despapelizacion.css')}}">
 
  {{-- css imports para el formato de pagina de la oficina --}}
  <link rel="stylesheet" href="{{url('/css/components/menu_based.css')}}">
  <link rel="stylesheet" href="{{url('/css/components/based.css')}}">

@endsection

@section('content')

  {{-- @include('despapelizacion.navbar.navbar') --}}
  
  @include('includes.header-despapelizacion')

<div class="container" style="min-height:700px">


  {{-- @include('despapelizacion.encuesta') --}}

  
  @include('despapelizacion.modals.describir-cards')
            
</div>

  @include('includes.footerwrap')

  @include('includes.pie')

  @include('includes.boton-subir')


@endsection

@section('scripts')


  @if( env('APP_ENV') === 'production' )

    <script src="{{url('/js/despapelizacion-prod.js')}}"></script>

  @else

    <script src="{{url('/js/despapelizacion.js')}}"></script>

  @endif

  <script src="{{url('/js/menu.js')}}"></script>

@endsection
