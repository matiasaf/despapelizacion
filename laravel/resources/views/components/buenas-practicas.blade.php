<div class="buenas_practicas_section">

    <br>
    
    <div class="row">
        
        <h2 class="font-title-organismos mx-auto">Buenas prácticas</h2>
        
    </div>
    
    <br>

    <div class="row">
        
        <div class="col-md-12">
            
            <div class="row" style="margin-left:2%">      
                
                {{-- tarjetas con las buenas practicas --}}
                
                <div  style="margin-top:3%" class="col-md-3 d-flex align-items-center" v-for="(buenaPractica, index) in buenasPracticas">
                    
                    <div 
                    id="grid-card"     
                    v-bind:class="{ 
                        'color-1': ((index == 0) || (index == 4)), 
                        'color-2': ((index == 1) || (index == 5)), 
                        'color-3': ((index == 2) || (index == 6)), 
                        'color-4': ((index == 3) || (index == 7)) 
                    }"     
                    @click="openModalBuenaPractica(buenaPractica)">
                        
                        <div class="card-body">
                            
                            <div class="font-card">
                                @{{buenaPractica.titulo}}                            
                                <br>
                                <i class="fa fa-plus-circle" style="color:white;" aria-hidden="true"></i>
                            </div>


                        
                        </div>
                        
                    </div>
                    
                </div>
                               
            </div>      
                           
        </div>
        
    </div>
    
</div>