<div class="row">

    <div class="col-md-12">

        <div class="jumbotron" id="main-card">

                <div class="row">

                    <div class="col-md-7">
                        <p class="font-main-card">

                            El programa de despapelización de la Administración Pública busca fomentar la despapelización en las oficinas públicas del
                            Estado, entendiendo al proceso como la disminución del uso del papel con el propósito de lograr
                            beneficios como la optimización del espacio físico ocupado para almacenar la documentación histórica
                            generada, la dinamización de los procedimientos administrativos y el cuidado del ambiente a través
                            de un uso responsable del recurso del papel, generando a su vez un entorno más saludable en las
                            tareas diarias para los trabajadores del Estado.
                        </p>
                    </div>
                    <div class="col-md-5">
                        
                        <img src="{{url('img/main_card_despapelizacion.png')}}" alt="" class="img-fluid"/>
                        
                        <button @click="habilitarModalInscripcion" type="button" style="margin-top:10px" class="btn btn-lg btn-block boton-sumate">
                            <div class="font-boton-main-card">
                                Sumate
                            </div>
                        </button>

                    </div>

                </div>

        </div>

    </div>

</div>