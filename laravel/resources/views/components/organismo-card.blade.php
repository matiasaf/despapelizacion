<div class="card card-organismo-2 animated fadeIn" style="width: 20rem;">
    <img class="card-img-top" :src="organismo.img_src" alt="Card image cap">
    <div class="card-body">
        <span
        style="margin-bottom: 15px; font-size:0.9em;"
        class="badge badge-pill badge-success">
          Fecha de adhesión : @{{ organismo.fecha_adhesion}}
        </span>
          <h5 class="card-title font-title-card"> @{{organismo.nombre}}</h5>
          <p class="card-text font-nexa-light" style="text-align: justify:">
                Institución que apadrina :
                @{{organismo.institucion_apadrina}}
          </p>
          <button
          @click="openModalCompromisos(organismo)"
          type="button"
          class="btn btn-md btn-block btn-secondary font-nexa">
          Ver adhesión
          </button>
          <button
          @click="descargarCompromisosOrganismo(organismo)"
          type="button"
          class="btn btn-md btn-block boton font-nexa">
          Descargar adhesión
          </button>
    </div>
</div>
