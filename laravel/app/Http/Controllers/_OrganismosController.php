<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Organismo;
use App\Votacion;
use App\VotacionFechas;

use Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrganismosController extends Controller
{

    public function store(Request $request)
    {
        $organismo = Organismo::create($request->all());

        $file = $request->file('pdf_compromisos');
        Storage::put('/' . $organismo->id . '/compromisos.pdf', file_get_contents($file));

        $imageData = $request->get('image');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];      
        
        Image::make($request->get('image'))->save('images/profiles/'.$fileName);

        $organismo->img_src = 'images/profiles/'.$fileName; 

        $organismo->save();
        
    }

    public function index()
    {
        $organismos = Organismo::orderBy('fecha_adhesion','desc')->get();

        return response()->json($organismos);
    }

    
    public function getOrganismosEncuesta(Request $request)
    {
        $organismos = Organismo::all();
        
        $votacion_fechas = VotacionFechas::where('activa',1)->first();

        $yavoto = false;
        $antes_inicio_votacion = false;
        $pos_fin_votacion = false;
        $organismo_ganador = [];

        //chequear si ya voto el usuario
        $votacion = Votacion::where('usuario_ip', $request->ip())
                                ->where('votacion_fechas_id',$votacion_fechas->id)
                                ->get();


        if (!$votacion->isEmpty()) {
            $yavoto = true;
        }

        $total_votacion_encuesta = Votacion::where('votacion_fechas_id',$votacion_fechas->id)->count();

        //chequear si el dia esta dentro de la votacion activa.


        $fecha_hoy = Carbon::now();
        $fecha_inicio = Carbon::createFromFormat('Y-m-d', $votacion_fechas->fecha_inicio);
        $fecha_fin = Carbon::createFromFormat('Y-m-d', $votacion_fechas->fecha_fin);

        if ($fecha_inicio > $fecha_hoy)
            $antes_inicio_votacion = true;        
        else if ($fecha_fin < $fecha_hoy)
            $pos_fin_votacion = true;

        $organismos_response = [];

        $total_votacion_total = Votacion::where('votacion_fechas_id','=',$votacion_fechas->id)
                                 ->count();

        foreach($organismos as $organismo){
            

            $total_votacion_organismo = Votacion::where('organismo_id','=',$organismo->id)
                                        ->where('votacion_fechas_id','=',$votacion_fechas->id)
                                        ->count();

            if($total_votacion_total == 0) $total_votacion_total = 1;
            
            $porcentaje =  round($total_votacion_organismo / $total_votacion_total * 100);                          
            
            $_organismo = [
                'id' => $organismo->id,
                'nombre' => $organismo->nombre, 
                'img_src' => $organismo->img_src, 
                'objetivos_descripcion' => $organismo->objetivos_descripcion, 
                'institucion_apadrina' => $organismo->institucion_apadrina, 
                'cantidad_votos' => $porcentaje 
            ];

            array_push($organismos_response, $_organismo);
        }

        if($pos_fin_votacion){
            
            $organismo_ganador = $organismos_response[0];

            foreach($organismos_response as $organismo){

                if($organismo['cantidad_votos'] > $organismo_ganador['cantidad_votos']){
                    $organismo_ganador = $organismo;
                }
                
            }
   
        }

        return response()->json([
            'organismos' => $organismos_response, 
            'organismo_ganador' => $organismo_ganador,
            'total_votos_encuesta' => $porcentaje,
            'yavoto' => $yavoto,
            'antes_inicio_votacion' => $antes_inicio_votacion,
            'pos_fin_votacion' => $pos_fin_votacion
        ]);
    }


    public function edit(Request $request)
    {
        $organismo = Organismo::find($request->id);

        $file = $request->file('pdf_compromisos');
    
        Storage::deleteDirectory( '/' . $organismo->id );
        Storage::put('/' . $organismo->id . '/compromisos.pdf', file_get_contents($file));
        
        $organismo->nombre = $request->nombre;
        $organismo->institucion_apadrina = $request->institucion_apadrina;
        $organismo->objetivos_descripcion = $request->objetivos_descripcion;
        $organismo->img_src = $request->img_src;
        $organismo->fecha_adhesion = $request->fecha_adhesion;
        
        $imageData = $request->get('image');

        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];      
        
        Image::make($request->get('image'))->save('images/profiles/'.$fileName);

        // guardar la ruta a la imagen en el efector
        $organismo->img_src = 'images/profiles/'.$fileName; 
               
        $organismo->save();
        
        return response()->json($organismo);

    }

    public function delete($organismoId)
    {
        $organismo = Organismo::find($organismoId);

        $organismo->delete();

        //ELIMINO LA CARPETA DONDE SE ALOJA EL PDF

        Storage::deleteDirectory( '/' . $organismoId );

        $organismos = Organismo::all();

        return response()->json($organismos);

    }

    public function descargarCompromisos(Request $request)
    {
        $file = storage_path() . "/app/" . $request->id. "/compromisos.pdf";

        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($file, 'compromisos.pdf', $headers);
    }

     public function guardarImagen($request, $organismo){        
        $imageData = $request->get('image');

        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
         
        Image::make($request->get('image'))->save('img/profiles/'.$fileName);

        // guardar la ruta a la imagen en el efector
        $organismo->img_src = 'img/profiles/'.$fileName;
     }

}
