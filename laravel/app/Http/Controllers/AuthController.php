<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Usuario;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


use Auth;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers;


    public function getRegisterPage()
    {
        return view('auth.register');
    }

    protected function crearUsuario(Request $request)
    {
        $rules =[
            'username' => 'required|max:255',
            'nombre' => 'required|max:255',
            'apellido' => 'required|max:255',
            'email' => 'required|email|max:255|unique:usuarios',
            'password' => 'require|confirmed|min:6'
        ];

        $messages=[
            'username.required' => 'Ingrese el nombre',
            'nombre.required' => 'Ingrese el nombre',
            'apellido.required' => 'Ingrese el nombre',

            'email.required' => 'Ingrese el mail',
            'email.unique' => 'El mail ya se encuentra registrado, por favor utilice otro.',

            'password.required' => 'Ingrese el password',
            'password.min' => 'Debe tener un mínimo de 6 caracteres!',
            'password.confirmed' => 'Las contraseñas deben coincidir.'
        ];

        // $this->validate($request, $rules, $messages);

        Usuario::insertGetId(
            array(
                        'username' => $request['username'],
                        'nombre' => $request['nombre'],
                        'apellido' => $request['apellido'],
                        'email' => $request['email'],
                        'password' => bcrypt($request['password'])
                  )
        );

        return redirect()->route('admin.view');
    }

    public function getLoginPage()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            return redirect()->route('admin.view');
        } else {
            return redirect()->route('auth.login.view')->with('message', 'Falló la autenticación');
        }
    }

    public function logout()
    {
      Auth::logout();

      return redirect()->route('auth.login.view');
    }
}
