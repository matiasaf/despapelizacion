<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Votacion;
use App\VotacionFechas;

class VotacionFechasController extends Controller
{
    public function index(){

        $votacion_fechas = VotacionFechas::orderBy('fecha_inicio','desc')->get();

        return response()->json($votacion_fechas);
        
    }

    public function store(Request $request){
        
        $votacion_fechas = new VotacionFechas();
        
        $votacion_fechas->nombre = $request->nombre;
        $votacion_fechas->fecha_inicio = $request->fecha_inicio;
        $votacion_fechas->fecha_fin = $request->fecha_fin;
        $votacion_fechas->activa = 0;
        
        $votacion_fechas->save();

        $votaciones = VotacionFechas::all();

        return response()->json($votaciones);

    }

    public function habilitar(Request $request){
        
        //primero seteo todas en 0 las activas

        $votacion_fechas_todas = VotacionFechas::all();

        foreach( $votacion_fechas_todas as $votacion){
            $votacion->activa = 0;
            $votacion->save();
        }

        $votacion_fechas = VotacionFechas::find($request->id);

        $votacion_fechas->activa = 1;

        $votacion_fechas->save();

        $votacion_fechas_todas = VotacionFechas::orderBy('fecha_inicio','desc')->get();


        return response()->json($votacion_fechas_todas);
    }

    public function edit(Request $request){

        $votacion_fechas = VotacionFechas::find($request->id);
        
        $votacion_fechas->nombre = $request->nombre;
        $votacion_fechas->fecha_inicio = $request->fecha_inicio;
        $votacion_fechas->fecha_fin = $request->fecha_fin;
        
        $votacion_fechas->save();

        $votaciones = VotacionFechas::orderBy('fecha_inicio','desc')->get();

        return response()->json($votaciones);

    }

    public function delete($idVotacion){

        $votacion_fechas = VotacionFechas::find($idVotacion);
        $votacion_fechas->delete();

    }

}