<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Votacion;
use App\VotacionFechas;

class VotacionController extends Controller
{
    public function getView()
    {
        return view('votacion.index');
    }

    public function store(Request $request)
    {
        $votacion_fechas = VotacionFechas::where('activa',1)->first();
        
        $votacion = Votacion::where('usuario_ip', $request->ip())
                                ->where('votacion_fechas_id','=',$votacion_fechas->id)
                                ->get();

        if (!$votacion->isEmpty()) {
            return response()->json(
                ['errors' => 'Usted ya ha votado en esta encuesta.'],
                500);
        }


        $votacion = new Votacion();

        $votacion->organismo_id = $request->organismo_id;
        $votacion->usuario_ip = $request->ip();
        $votacion->votacion_fechas_id = $votacion_fechas->id;

        $votacion->save();

    }

    public function comprobarIp(Request $request){
       
        $votacion = Votacion::where('usuario_ip', $request->ip())->get();

        if (!$votacion->isEmpty()) {
            return response()->json(
                ['errors' => 'Usted ya ha votado en esta encuesta.'],
                500);
        }

    }

}
