<?php

namespace App\Http\Controllers;

use App\TextoTarjetaVerde;

use Illuminate\Http\Request;

class TextoTarjetaVerdeController extends Controller
{

    public function get(){

        $texto_tarjeta = TextoTarjetaVerde::get()[0];

        return response()->json($texto_tarjeta);
    
    }

    public function update(Request $request){
        
        $texto_tarjeta = TextoTarjetaVerde::get()[0];

        $texto_tarjeta->texto = $request->texto;

        $texto_tarjeta->save();
        
    }

}