<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;


class DespapelizacionController extends Controller
{
    public function getView()
    {
        return view('index');
    }

    public function getAdminPage()
    {
        return view('admin.index');
    }

    public function getAdhesionView()
    {
        return view('despapelizacion.adhesion.index');
    }

    public function obtenerIp(Request $request)
    {
        return response()->json($request->ip());
    }

    public function enviarMail(Request $request)
    {
        // enviar mail con pdf adjunto
        $data['name'] = 'Despapelizacion';
        $data['email'] = 'fernandez.amatias@gmail.com';
        $data['nombre_organismo_formulario'] = $request->nombre_organismo_formulario;
        $data['autoridad_organismo_formulario'] = $request->autoridad_organismo_formulario;
        $data['persona_contacto_formulario'] = $request->persona_contacto_formulario;
        $data['correo_electronico_formulario'] = $request->correo_electronico_formulario;
        $data['telefono_contacto_formulario'] = $request->telefono_contacto_formulario;
        $data['titulo_compromiso_formulario'] = $request->titulo_compromiso_formulario;
        $data['detalle_compromiso_formulario'] = $request->detalle_compromiso_formulario;

        Mail::send('components.correo.formulario', ['data' => $data], function ($m) use ($data) {
            $m->to($data['email'], $data['name'])
              ->from('secmodernizacioncyt@entrerios.gov.ar')
              ->subject('Se ha sumado un nuevo organismo');
        });
    }
}
