<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Efector;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function descargarDocumentacion(Request $request)
    {
        $file = storage_path()."/app/efectores/".$request->cuit;

        $headers = array(
                'Content-Type: application/zip',
              );

        return response()->download($file, 'documentacion.zip', $headers);
    }
}
