<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// ROUTES DESPAPELIZACION

Route::get('/', [
    'uses' => 'DespapelizacionController@getView',
    'as' => 'despapelizacion.view'
]);

Route::post('obtener_ip', [
    'uses' => 'DespapelizacionController@obtenerIp',
    'as' => 'despapelizacion.obtener_ip'
]);

Route::get('auth/login', [
    'uses' => 'AuthController@getLoginPage',
    'as' => 'auth.login.view'
]);

Route::get('register', [
    'uses' => 'AuthController@getRegisterPage',
    'as' => 'auth.register.view'
]);

Route::post('auth/register', [
    'uses' => 'AuthController@crearUsuario',
    'as' => 'auth.register'
]);

Route::post('auth/login', [
    'uses' => 'AuthController@login',
    'as' => 'auth.login'
]);

Route::get('auth/logout', [
    'uses' => 'AuthController@logout',
    'as' => 'auth.logout'
]);

Route::post('votacion/comprobar_ip',[
    'uses' => 'VotacionController@comprobarIp',
    'as' => 'votacion.comprobar_ip'
]);

Route::post('enviar_formulario',[
    'uses' => 'DespapelizacionController@enviarMail',
    'as' => 'enviar_formulario'
]);

// rutas de los organismos

Route::get('organismos', [
    'uses' => 'OrganismosController@index',
    'as' => 'organismos.index'
]);

Route::post('/compromisos/descargar', [
    'uses' => 'OrganismosController@descargarCompromisos',
    'as' => 'compromisos.descargar'
]);

//rutas de la votacion

Route::post('votacion/organismos',[
    'uses' => 'OrganismosController@getOrganismosEncuesta',
    'as' => 'votacion.organismos'
]);
Route::get('votacion',[
    'uses' => 'VotacionController@getView',
    'as' => 'votacion.view'
]);

Route::post('votacion/store',[
    'uses' => 'VotacionController@store',
    'as' => 'votacion.store'
]);

Route::post('votacion/comprobar_ip',[
    'uses' => 'VotacionController@comprobarIp',
    'as' => 'votacion.comprobar_ip'
]);


//rutas texto tarjeta verde

Route::get('texto_tarjeta_verde',[
    'uses' => 'TextoTarjetaVerdeController@get',
     'as'  =>'texto_tarjeta_verde'
]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/', [
        'uses' => 'DespapelizacionController@getAdminPage',
        'as' => 'admin.view',
    ]);

    Route::post('organismos/add', [
        'uses' => 'OrganismosController@store',
        'as' => 'organismos.add',
    ]);

    Route::post('organismos/edit', [
        'uses' => 'OrganismosController@edit',
        'as' => 'organismos.edit',
    ]);

    Route::delete('organismos/delete/{organismoId}', [
        'uses' => 'OrganismosController@delete',
        'as' => 'organismos.delete',
    ]);

    Route::post('votacion/agregar_fechas',[
        'uses' => 'VotacionFechasController@store',
        'as' =>'votacion.agregar_fechas'
    ]);

    Route::put('votacion/habilitar_fecha',[
        'uses' => 'VotacionFechasController@habilitar',
        'as' =>'votacion.habilitar_fecha'
    ]);
    
    Route::get('votacion/get_lista',[
        'uses' => 'VotacionFechasController@index',
        'as' =>'votacion.get_lista'
    ]);
    
    Route::delete('votacion/delete/{idVotacion}',[
        'uses' => 'VotacionFechasController@delete',
        'as'   =>'votacion.delete'
    ]);

    Route::put('votacion/edit',[
        'uses' => 'VotacionFechasController@edit',
        'as'   =>'votacion.edit'
    ]);

    //rutas texto tarjeta verde

    Route::put('texto_tarjeta_verde',[
        'uses' => 'TextoTarjetaVerdeController@update',
        'as'  =>'texto_tarjeta_verde.update'
    ]);

});
