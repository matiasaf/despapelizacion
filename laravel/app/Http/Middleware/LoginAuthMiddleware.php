<?php

namespace App\Http\Middleware;

use Closure;

class LoginAuthMiddleware
{
    public function handle($request, Closure $next)
    {
        if(session('logeado') !== true)
        {
        	return redirect('login');
        }
        return $next($request);
    }
}