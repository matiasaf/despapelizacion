<?php

namespace App\Http\Middleware;

use Closure;

class CiudadanoRolMiddleware
{
    public function handle($request, Closure $next)
    {
        if(session('rol') != 1)
        {
        	return redirect('/');
        }
        return $next($request);
    }
}