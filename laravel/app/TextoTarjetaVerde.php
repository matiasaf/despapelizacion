<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextoTarjetaVerde extends Model
{
    protected $table = 'laravel_texto_tarjeta_verde';

    public $timestamps = false;

}