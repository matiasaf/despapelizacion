<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotacionFechas extends Model
{
    protected $table = 'laravel_votacion_fechas';

    public $timestamps = false;

}
