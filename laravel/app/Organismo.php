<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organismo extends Model
{
    protected $table = 'laravel_organismos';

    protected $fillable = [
        'nombre',
        'institucion_apadrina',
        'objetivos_descripcion',
        'fecha_adhesion'
    ];

    public $timestamps = false;

}
