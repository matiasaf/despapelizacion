<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votacion extends Model
{
    protected $table = 'laravel_votacion';

    public $timestamps = false;

}
