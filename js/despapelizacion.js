var vm = new Vue({
  el: "#manage-vue",

  data: function() {
    return {
      
      green_card_texto: '',

      // variables de navegacion
      sabias_que_active: false,
      buenas_practicas_active: false,
      organismos_active: false,

      modalCard: {},
      pagina: 0,
      total_paginas: 0,
      carrousel: '',

      buenasPracticas: [
        {
          titulo: "Usar el papel de forma racional",
          descripcion:
            "Fotocopiar e imprimir en doble faz. Cuando se utilizan las dos caras de una hoja se ahorra papel y se reducen los envíos, el peso de los archivos, se optimiza el espacio de almacenamiento y los documentos son más fáciles de abrochar, encarpetar y transportar. Es recomendable que en la tercerización o contratos de servicios de fotocopiado e impresión, así como en la adquisición de fotocopiadoras y/o impresoras se seleccionen aquellas que sean multifunción e impriman doble faz (dúplex) de forma automática."
        },
        {
          titulo: "Editar los documentos",
          descripcion: `
          <b>Elegir fuente y tamaño pequeño:</b> elegir el tipo de letra más pequeño posible en la impresión de
          borradores (por ejemplo 10/11 puntos).
          <br>
          <b>Configurar de forma correcta:</b> muchas de las impresiones fallidas se deben a la falta de verificación de la
          configuración de los documentos antes de dar la orden de impresión. Para evitar estos desperdicios de
          papel es importante utilizar las opciones de revisión y vista previa para identificar elementos fuera de los
          márgenes. En el caso de los borradores o documentos internos pueden usarse márgenes más
          estrechos.
          <br>
          <b>Revisión y ajuste de los formatos:</b> optimizar el uso del espacio en los formatos seleccionados. Es
          necesaria la revisión de los procedimientos para identificar la posibilidad de integrar varios documentos
          o formatos en uno solo y reducir el número de copias elaboradas.
          <br>
          <b>Lectura y corrección en pantalla:</b> corregir un documento durante su elaboración entre dos y tres veces
          antes de su versión definitiva. De esta manera solo se imprime la versión final del documento.
          `
        },
        {
          titulo: "Evitar copias e impresiones innecesarias",
          descripcion: `
          Antes de crear o generar múltiples ejemplares de un mismo documento determinar si son realmente
          indispensables. En la mayoría de los casos existen medios alternativos digitales para compartir o
          guardar copias de apoyo como las carpetas compartidas online, el correo electrónico, la intranet y los
          repositorios. Dejar de lado la Impresión de correos electrónicos a menos que sean de importancia 
          `
        },
        {
          titulo: "Guardar los archivos",
          descripcion: `
            En los casos en que no se requiera una copia impresa de los documentos se recomienda guardarlos en
            el disco duro de la computadora u otros sistemas de almacenamiento masivo (como un pen drive) o
            MANUAL DE BUENAS PRÁCTICAS
            medio tecnológico que permita conservar temporalmente la información. Es importante que las
            entidades cuenten con políticas claras para nombrar o rotular, clasificar y sobre la disposición de
            documentos digitales con el fin de que puedan ser preservados así como garantizar su recuperación y
            acceso para consulta     
            `
        },

        {
          titulo: "Conocer el funcionamiento de los equipos tecnológicos",
          descripcion: `
          Es importante que todos los trabajadores del Estado conozcan el correcto funcionamiento de
          impresoras, fotocopiadoras y multifuncionales para evitar el desperdicio de papel derivado de errores
          en su uso. De ser necesario, pueden realizarse sesiones de asistencia técnica sobre el manejo de estos
          equipos.  
          `
        },
        {
          titulo: "Reutilizar el papel",
          descripcion: `
          Las hojas de papel ya utilizadas y que son de descarte pueden reutilizarse como borradores,
          anotadores, para la impresión de formatos a diligenciar de forma manual, para elaborar listas de
          asistencia, entre otros usos posibles.
          `
        },
        {
          titulo: "Usar el correo electrónico",
          descripcion: `
            El correo electrónico es primordial a la hora de compartir información evitando el uso de papel. Es
            necesario que las entidades establezcan y promuevan políticas de uso apropiado entre los servidores
            públicos para evitar que se transformen en repositorios de basura digital. Es recomendable no imprimir
            los correos a menos que sea indispensable. En ese caso, una buena práctica es eliminar el contenido que
            no aporte información al mensaje principal.
            `
        },
        {
          titulo: "Promover herramientas de colaboración",
          descripcion: `
            Teniendo en cuenta las medidas de seguridad necesarias para resguardar la información se sugiere
            crear y promover el uso de espacios virtuales de trabajo como los programas de mensajería
            instantánea, las aplicaciones en línea para teleconferencias y edición de 
            documentos/calendarios/imágenes compartidas, ya que ofrecen oportunidades reales para el
            intercambio de forma rápida y efectiva y evitan la utilización de papel.
            `
        }
      ],
      organismos: [],

      organismoSeleccionado:'',
      organismoModal:'',
      mostrar_organismos: false,

      //formulario de adhesion
      nombre_organismo_formulario: '',
      autoridad_organismo_formulario: '',
      persona_contacto_formulario: '',
      correo_electronico_formulario: '',
      telefono_contacto_formulario: '',
      titulo_compromiso_formulario: '',
      detalle_compromiso_formulario: '',
      
      votacion_active: false,

        // para mostrar el boton al inicio
      votacion_activa: false,
    };
  },
  methods: {
    getOrganismos: function() {
      axios
        .get('./organismos')
        .then( res => {
          
          var cant = 0;
          let organismos=[];
          let organismos_pagina=[];

          for(var i=0; i<res.data.length; i++)
          {

            organismos_pagina.push(res.data[i]);
            
            cant = cant + 1;
            
            if(cant === 3){           
            
              cant = 0;
              this.$set(this,'pagina', this.pagina + 1 );  
              organismos.push(organismos_pagina);
              organismos_pagina = [] ;   
            
            }


          }

          organismos.push(organismos_pagina);

          this.$set(this,'organismos', organismos);

          this.$set(this,'pagina', 0 );
          
          this.$set(this, 'total_paginas', Math.floor(res.data.length / 3) + 1 );

          this.$set(this, 'mostrar_organismos', true);
        
        });
    },
    openModalBuenaPractica: function(buenaPractica) {
      this.$set(this, "modalCard", buenaPractica);

      $("#descripcionCardsModal").modal("show");
    },
    levantarCarrouselcasero: function() {
      
      let that = this;

      this.carrousel = setInterval(function() {
        var modulo = (that.pagina + 1) % that.total_paginas ;

        that.$set(that, "pagina", modulo);
      }, 9000);

    },
    avanzoCarrousel: function() {
      var modulo = (this.pagina + 1) % this.total_paginas;

      this.$set(this, "pagina", modulo);
    },
    retrazoCarrousel: function() {
      let modulo = 0;

      if (this.pagina == 0) modulo = this.total_paginas - 1;
      else modulo = (this.pagina - 1) % this.total_paginas;

      this.$set(this, "pagina", modulo);
    },
    scrollToOrganismos: function() {
      this.activoOrganismos();

      $("html, body").animate(
        {
          scrollTop: $(".organismos_section").offset().top
        },
        1000
      );
    },
    scrollToBuenasPracticas: function() {
      this.activoBuenasPracticas();

      $("html, body").animate(
        {
          scrollTop: $(".buenas_practicas_section").offset().top - 60
        },
        1000
      );
    },
    scrollToSabiasQue: function() {
      this.activoSabiasQue();

      $("html, body").animate(
        {
          scrollTop: $(".sabias_que_section").offset().top - 90
        },
        1000
      );
    },
    habilitarModalInscripcion: function() {
      $("#adhesionFormModal").modal("show");
    },
    activoBuenasPracticas: function() {
      this.$set(this, "sabias_que_active", false);
      this.$set(this, "buenas_practicas_active", true);
      this.$set(this, "organismos_active", false);
    },
    activoOrganismos: function() {
      this.$set(this, "sabias_que_active", false);
      this.$set(this, "buenas_practicas_active", false);
      this.$set(this, "organismos_active", true);
    },
    activoSabiasQue: function() {
      this.$set(this, "sabias_que_active", true);
      this.$set(this, "buenas_practicas_active", false);
      this.$set(this, "organismos_active", false);
    },
    isInViewport: function(elem) {
      let elemTop = elem.offsetTop;
      let elemBottom = elemTop + elem.offsetHeight;
      let viewportTop = window.scrollY;
      let viewportBottom = viewportTop + window.innerHeight;

      return elemBottom > viewportTop && elemTop < viewportBottom;
    },
    comprueboViewport: function() {
      var that = this;

      var organismo_section = $(".organismos_section")[0];
      var sabias_que_section = $(".sabias_que_section")[0];
      var buenas_practicas_section = $(".buenas_practicas_section")[0];

      setInterval(function() {
        if (that.isInViewport(organismo_section)) {
          that.activoOrganismos();
        } else if (that.isInViewport(sabias_que_section)) {
          that.activoSabiasQue();
        } else if (that.isInViewport(buenas_practicas_section)) {
          that.activoBuenasPracticas();
        }
      }, 200);

    },
    descargarCompromisosOrganismo: function(organismo){
        
        this.$set(this, "organismoSeleccionado", organismo);
  
        setTimeout(() => $("#form_descargar_compromisos").submit(), 500);

    },
    openModalCompromisos: function(organismo){
      
      this.$set(this, "organismoModal", organismo);

      $("#compromisosModal").modal("show");

      clearInterval(this.carrousel);

    },
    getTextoTarjetaVerde: function(){
      
      axios.get('./texto_tarjeta_verde')
        .then(res => this.$set( this,'green_card_texto', res.data.texto));

    },
    goToVotaciones: function(){
      
      location.href = location.href + 'votacion' ;

    },
    enviarFormulario : function(){

      let formulario = {
        nombre_organismo_formulario:  this.nombre_organismo_formulario,
        autoridad_organismo_formulario:  this.autoridad_organismo_formulario,
        persona_contacto_formulario:  this.persona_contacto_formulario,
        correo_electronico_formulario:  this.correo_electronico_formulario,
        telefono_contacto_formulario:  this.telefono_contacto_formulario,
        titulo_compromiso_formulario:  this.titulo_compromiso_formulario,
        detalle_compromiso_formulario:  this.detalle_compromiso_formulario,
      }

      axios.post('./enviar_formulario', formulario)
        .then(res =>  {
          
          toastr.success( "Se enviaron correctamente los datos", "¡Operación exitosa!")
          
          $("#adhesionFormModal").modal("hide");

          this.limpiarCamposFormulario();

      })
    },
    limpiarCamposFormulario: function(){

      this.$set(this,'nombre_organismo_formulario', '');
      this.$set(this,'autoridad_organismo_formulario', '');
      this.$set(this,'persona_contacto_formulario', '');
      this.$set(this,'correo_electronico_formulario', '');
      this.$set(this,'telefono_contacto_formulario', '');
      this.$set(this,'titulo_compromiso_formulario', '');
      this.$set(this,'detalle_compromiso_formulario', '');

    },

    votacionActiva: function(){
      axios
      .post("./votacion/organismos")
      .then(res => 
        this.$set(this, 'votacion_activa', (!res.data.antes_inicio_votacion && !res.data.pos_fin_votacion))
      )
    }
  },
  mounted: function() {
    // location.reload();
    this.votacionActiva();
    this.getTextoTarjetaVerde();
    this.getOrganismos();
    this.levantarCarrouselcasero();
    this.comprueboViewport();
  },

  watch: {

  },
  computed: {
  }
});
