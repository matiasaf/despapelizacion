window.departamentos = [
  {
    nombre: 'Colón',
    localidades: [
      {
        nombre: 'Colón'
      }, {
        nombre: 'San Jose'
      }, {
        nombre: 'Villa Elisa'
      }, {
        nombre: 'Ubajay'
      }, {
        nombre: 'Pueblo Liebig'
      }
    ]
  }, {
    nombre: 'Concordia',
    localidades: [
      {
        nombre: 'Barrios de Concordia'
      }, {
        nombre: 'Concordienses‎'
      }, {
        nombre: 'Deporte en Concordia'
      }, {
        nombre: 'Concordia'
      }, {
        nombre: 'Cine Gran Odeón'
      }, {
        nombre: 'Historia de Concordia'
      }, {
        nombre: 'Jardín botánico Ca´a Porâ'
      }, {
        nombre: 'Regimiento de Caballería de Tanques'
      }, {
        nombre: 'San Antonio del Salto Chico'
      }
    ]
  }, {
    nombre: 'Diamante',
    localidades: [
      {
        nombre: 'Diamante'
      }, {
        nombre: 'Estación Puerto Diamante'
      }, {
        nombre: 'Estación Strobel'
      }, {
        nombre: 'Parque nacional Pre-delta'
      }, {
        nombre: 'Strobel'
      }
    ]
  }, {
    nombre: 'Federación'
  }, {
    nombre: 'Federal'
  }, {
    nombre: 'Feliciano'
  }, {
    nombre: 'Gualeguay'
  }, {
    nombre: 'Gualeguay'
  }, {
    nombre: 'Gualeguaychú'
  }, {
    nombre: 'Islas del Ibicuy'
  }, {
    nombre: 'La Paz',
    localidades: [
      {
        nombre: 'La Paz'
      }, {
        nombre: 'Alcaraz'
      }, {
        nombre: 'Bovril'
      }, {
        nombre: 'Santa Elena'
      }, {
        nombre: 'Piedras Blancas'
      }, {
        nombre: 'San Gustavo'
      }, {
        nombre: 'Colonia Viraró - Colonia Avigdor'
      }, {
        nombre: 'El Solar'
      }, {
        nombre: 'Sir Leonar'
      }, {
        nombre: 'Tacuara Ombú - Las Toscas - San Ramirez'
      }, {
        nombre: 'Puerto Algarrobo'
      }, {
        nombre: 'TACUARAS YACARÉ-COLONIA OFICIAL Nro.3 Y 14'
      }, {
        nombre: 'COLONIA CARRASCO'
      }, {
        nombre: 'LA PROVIDENCIA-ALCARAZ NORTE-ALCARAZ SUR'
      }, {
        nombre: 'PICADA BERÓN-YESO OESTE'
      }, {
        nombre: 'EL QUEBRACHO'
      }, {
        nombre: 'COLONIA OFICIAL Nro.13-ESTAQUITAS-SAUCESITO'
      }
    ]
  }, {
    nombre: 'Nogoyá'
  }, {
    nombre: 'Paraná'
  }, {
    nombre: 'San Salvador'
  }, {
    nombre: 'Tala'
  }, {
    nombre: 'Uruguay'
  }, {
    nombre: 'Victoria'
  }, {
    nombre: 'Villaguay'
  }
]
