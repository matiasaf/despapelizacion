var vm = new Vue({
  el: "#manage-vue",

  data: function() {
    return {
      option1: 0,
      option2: 0,

      // total:8,

      modalCard: {},

      organismos: [],

      organismoSeleccionado: "",
      organismoModal: "",
      mostrar_organismos: false,
      mostrarInstitucionesAVotar: false,
      habilitoMensajeAntesDeVotacion: false,
      habilitoMensajePosVotacion: false,

      organismo_ganador: '',
      habilitoGrafico: false,

      habilitoObjetivos:false,

      total_votos_encuesta: ''
    };
  },
  methods: {
    getOrganismos: function() {

      axios
        .post("./votacion/organismos")
        .then(res => {
          this.$set(this, "organismos", res.data.organismos)
          
          if(res.data.antes_inicio_votacion)
          {
            this.$set(this, 'habilitoMensajeAntesDeVotacion', true)
          
          } else if(res.data.pos_fin_votacion){
            
            this.$set(this,'habilitoMensajePosVotacion', true)
            this.$set(this,'organismo_ganador',res.data.organismo_ganador)
            this.$set(this,'total_votos_encuesta', res.data.total_votos_encuesta)
            
          } else if(res.data.yavoto) {

            this.$set(this, 'habilitoGrafico', true)
            this.$set(this,'textoYaVoto', 'Usted ya voto en la encuesta.')  
          
          } else {  

            this.$set(this, 'mostrarInstitucionesAVotar', true)
         
          }
        
        })
        .then(() => this.createChart())
    },
    
    descargarCompromisosOrganismo: function(organismo) {
      this.$set(this, "organismoSeleccionado", organismo);

      setTimeout(() => $("#form_descargar_compromisos").submit(), 500);
    },
    openModalCompromisos: function(organismo) {
      this.$set(this, "organismoModal", organismo);

      $("#compromisosModal").modal("show");
    },
    votar: function(organismo) {

      let _organismos = this.organismos.map(_organismo => {
        if (_organismo.id === organismo.id)
          return { ..._organismo, voto: true };
        else 
          return { ..._organismo, voto: false };
      });

      this.$set(this, "organismos", _organismos);

    },
    getRandomColor: function() {
      var letters = '0123456789ABCDEF';
      var color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    },
    createChart: function() {
      
      let labels_organismos = this.organismos.map(organismo => organismo.institucion_apadrina);

      let cantidad_votos = this.organismos.map(organismo => organismo.cantidad_votos);
     
      let colores_barras = this.organismos.map(organismo => this.getRandomColor() );
      let colores_bordes = this.organismos.map(organismo => '#505050' );
      
      var chartOptions = {
        responsive: true,
        scales: {
          yAxes: [{
            barPercentage: 0.5,
            ticks:{
              fontSize: 10
            }
          }],
          xAxes: [{
            ticks: {
              min: 0,
              stepSize: 1,
            },
          }],
        },
        elements: {
          rectangle: {
            borderSkipped: 'left',
          }
        }
      };
       

    var densityData = {
      label: 'Resultados por institución ( % )',
      data: cantidad_votos,
      backgroundColor: colores_barras,
      borderColor: colores_bordes,
      borderWidth: 2,
      hoverBorderWidth: 0
    };

      var ctx = document.getElementById("myChart").getContext("2d");
      var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
          labels: labels_organismos,
          datasets: [densityData],
        },
        options: chartOptions
      });

    },
    guardarVotacion: function(){
        
        let organismo_elegido = '';
        
        this.organismos.forEach(organismo => {
            if (organismo.voto) organismo_elegido = organismo;
        });

        if(!organismo_elegido) toastr.error('Debe seleccionar un organismo','¡Hubo un error!')
        else{

            axios.post('votacion/store', {organismo_id : organismo_elegido.id})
            .then(res => {
              this.getOrganismos();
              toastr.success('Organismo votado con éxito','¡Operación exitosa!');
              this.$set(this, 'mostrarInstitucionesAVotar', false);
              this.$set(this, 'habilitoGrafico', true);
            })
            .catch(err => toastr.error(err.response.data.errors,'¡Hubo un error!'))
            
        }
    },
    volverEncuesta: function(){

      this.$set(this, 'habilitoGrafico', false);

    },
    volverAlSitio: function(){
      
      location.href = location.href.slice(0, -9);

    },
    traerObjetivos : function(){
      console.log('habilito');
      this.$set(this,'habilitoObjetivos', true);
    }
  },
  mounted: function() {
    // location.reload();
    this.getOrganismos();
    // this.comprobarSiVoto();
    // this.createChart();
  },

  watch: {},
  computed: {
    // a computed getter
    option1width: function() {
      return (this.option1 * 100) / this.total + "%";
    },
    option2width: function() {
      return (this.option2 * 100) / this.total + "%";
    },
    total: function() {
      return this.option1 + this.option2;
    }
  }
});
