var vm = new Vue({
  el: "#manage-vue",

  data: function() {
    return {
      errores: {},

      organismos: [],
      lista_votaciones: [],
      organismoEdit: "",

      // organismos
      nombre: "",
      institucion_apadrina: "",
      objetivos_descripcion: "",
      img_src: "",
      image: '',
      fecha_adhesion: "",

      // organismo editar
      organismoEdit_id: "",
      organismoEdit_nombre: "",
      organismoEdit_institucion_apadrina: "",
      organismoEdit_objetivos_descripcion: "",
      organismoEdit_img_src: "",
      organismoEdit_fecha_adhesion: "",

      //votacion fechas
      votacion_fecha_inicio:'',
      votacion_fecha_fin:'',
      votacion_nombre: '',

      //paginacion
      currentPage: 1,
      lastPage: "",


      //votacion editar
      editar_votacion_id:'',
      editar_votacion_nombre: '',
      editar_votacion_fecha_inicio: '',
      editar_votacion_fecha_fin:'',


      texto_tarjeta_verde : ''
    };
  },

  methods: {
    getOrganismos: function() {
      axios
        .get("organismos")
        .then(res => this.$set(this, "organismos", res.data));
    },
    getListaVotaciones: function(){
      axios.get('./admin/votacion/get_lista')
      .then((res) => this.$set(this,'lista_votaciones',res.data));
    },
    openModalAgregarOrganismo: function() {
      $("#agregarOrganismoModal").modal("show");
    },
    openModalAgregarVotacion: function() {
      $("#agregarVotacionModal").modal("show");
    },
    openModalHabilitarVotacion: function() {
      $("#habilitarListaVotacion").modal("show");
    },
    agregarOrganismo: function() {
      let data = new FormData();

      data.append("pdf_compromisos", $("input[type='file']")[3].files[0]);
      data.append("nombre", this.nombre);
      data.append("institucion_apadrina", this.institucion_apadrina);
      data.append("objetivos_descripcion", this.objetivos_descripcion);

      data.append("image", this.image);
      data.append("fecha_adhesion", this.fecha_adhesion);

      axios.post("./admin/organismos/add", data, {
        headers: { "Content-Type": "multipart/form-data" }
      }).then(res => {
        
        this.getOrganismos();

        toastr.success(
          "Se agrego el organismo correctamente.",
          "¡Operación exitosa!"
        );
        $("#agregarOrganismoModal").modal("hide");
      });
    },
    agregarVotacion: function(){

      axios.post('./admin/votacion/agregar_fechas', {
        nombre : this.votacion_nombre,
        fecha_inicio : this.votacion_fecha_inicio,
        fecha_fin : this.votacion_fecha_fin
      })
      .then(res => {
        
        this.$set(this,'lista_votaciones', res.data);
        
        toastr.success(
          "Se la votación correctamente.",
          "¡Operación exitosa!"
        );

        $("#agregarVotacionModal").modal("hide");
      
      })

    },
    editarOrganismo: function() {
      let data = new FormData();

      data.append("pdf_compromisos", $("input[type='file']")[1].files[0]);
      data.append("id", this.organismoEdit_id);
      data.append("nombre", this.organismoEdit_nombre);
      data.append(
        "institucion_apadrina",
        this.organismoEdit_institucion_apadrina
      );
      data.append(
        "objetivos_descripcion",
        this.organismoEdit_objetivos_descripcion
      );

      data.append("image", this.image);

      data.append("fecha_adhesion", this.organismoEdit_fecha_adhesion);

      axios
        .post("./admin/organismos/edit", data, {
          headers: { "Content-Type": "multipart/form-data" }
        })
        .then(res => {
          
          let _organismos = this.organismos.map(organismo => {
            if (organismo.id == res.data.id) return res.data;
            else return organismo;
          });

          this.$set(this, "organismos", _organismos);

          toastr.success(
            "Se modificó el organismo correctamente.",
            "¡Operación exitosa!"
          );

          $("#editarOrganismoModal").modal("hide");
        });
    },
    openModalEditarOrganismo: function(organismo) {
      this.$set(this, "organismoEdit_id", organismo.id);
      this.$set(this, "organismoEdit_nombre", organismo.nombre);
      this.$set(
        this,
        "organismoEdit_institucion_apadrina",
        organismo.institucion_apadrina
      );
      this.$set(
        this,
        "organismoEdit_objetivos_descripcion",
        organismo.objetivos_descripcion
      );
      this.$set(this, "organismoEdit_fecha_adhesion", organismo.fecha_adhesion);
      this.$set(this, "organismoEdit_img_src", organismo.img_src);

      $("#editarOrganismoModal").modal("show");
    },
    eliminarOrganismo: function(organismo) {
      axios.delete(`./admin/organismos/delete/${organismo.id}`).then(res => {
        this.$set(this, "organismos", res.data);
        toastr.success(
          "Se eliminó correctamente el organismo.",
          "¡Operación exitosa!"
        );
      });
    },

    // methods para subir imagenes

    onFileChange(e) {
          
      let files = e.target.files || e.dataTransfer.files;
      if (!files.length)
      return;
      this.createImage(files[0]);
      this.$set(this, 'organismoEdit_img_src', '');

    },
    createImage(file) {
      let reader = new FileReader();
      let vm = this;
      reader.onload = (e) => {
          vm.image = e.target.result;
      };
      reader.readAsDataURL(file);
    },
    habilitarVotacion: function(votacion){
      
      axios.put('./admin/votacion/habilitar_fecha', votacion)
      .then((res) => {
        let votaciones = this.lista_votaciones;
        
        this.$set(this,'lista_votaciones', res.data);
      })
    },
    eliminarVotacion: function(votacion){
      axios.delete(`./admin/votacion/delete/${votacion.id}`)
      .then((res) => {
        let _lista_votaciones = this.lista_votaciones.filter( _votacion => votacion.id !== _votacion.id);
        
        this.$set(this,'lista_votaciones', _lista_votaciones);
      });
    },
    getTextoTarjetaVerde: function(){
      axios.get('./texto_tarjeta_verde')
        .then(res => this.$set( this,'texto_tarjeta_verde', res.data.texto));
    },
    editarTextTarjetaVerde: function(){
      axios.put('./admin/texto_tarjeta_verde', { texto: this.texto_tarjeta_verde })
      .then(res => toastr.success('Texto modificado correctamente.','¡Operación exitosa!'));
    },

    openModalEditar: function(votacion){
      
      this.$set(this,'editar_votacion_id', votacion.id);
      this.$set(this,'editar_votacion_nombre', votacion.nombre);
      this.$set(this,'editar_votacion_fecha_inicio', votacion.fecha_inicio);
      this.$set(this,'editar_votacion_fecha_fin', votacion.fecha_fin);

      $("#editarVotacionModal").modal("show");

    },
    editarVotacion: function(){

      let votacion = {
        id: this.editar_votacion_id,
        nombre: this.editar_votacion_nombre,
        fecha_inicio: this.editar_votacion_fecha_inicio,
        fecha_fin: this.editar_votacion_fecha_fin
      };

      axios.put('./admin/votacion/edit',votacion)
        .then((res) => {
          
          this.$set(this,'lista_votaciones', res.data);

          $("#editarVotacionModal").modal("hide");

          toastr.success('La votación fue modificada correctamente','¡Operación exitosa!')
          
        })
    },
    darFormato: function(fecha){
      return moment(fecha).format('DD-MM-YYYY');
    }

  },
    mounted: function() {
      this.getTextoTarjetaVerde();
      this.getOrganismos();
      this.getListaVotaciones();

    }
});
